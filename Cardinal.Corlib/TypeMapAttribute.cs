using System;

namespace Cardinal.Corlib
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class TypeMapAttribute : Attribute
    {
        public Type TargetType { get; }
        public TypeMapAttribute(Type targetType)
        {
            TargetType = targetType;
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class NativeImplAttribute : Attribute
    {
        public NativeImplAttribute()
        {
        }
    }
}
