﻿using System;

namespace Cardinal.Corlib
{
    [TypeMap(typeof(object))]
    public class Object
    {
        public Object()
        {

        }

        public Object(Object o)
        {

        }

        public Type GetType()
        {
            return null;
        }

        public Object MemberwiseClone()
        {
            return new Object(this);
        }
    }
}
