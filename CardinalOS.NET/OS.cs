﻿using System;

namespace CardinalOS.NET
{
    public class OS
    {
        public int Q = 0;

        public static void Main(IntPtr addr)
        {
            int a = 0;
            for (a = 0; a < 50; a++)
                a *= 2;
        }

        public OS()
        {
            Q = 1;
        }

        public static double B(float a, double b)
        {
            return  a * b + 0.2;
        }

        public static double A(float a, double b)
        {
            var q = 5.0;
            var c = q / 2.0 * 1.1;

            return c;
        }

        public override string ToString()
        {
            return "OS";
        }
    }
}
