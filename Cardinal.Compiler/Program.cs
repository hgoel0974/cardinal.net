﻿using System.Reflection;
using System;
using System.IO;
using Cardinal.Compiler.Platforms.x86_64;
using Cardinal.Corlib;
using System.Collections.Generic;

namespace Cardinal.Compiler
{
    class Program
    {
        static void Main(string[] args)
        {
            var dll = new FileInfo(args[0]);
            var assembly = Assembly.LoadFile(dll.FullName);

            TypeDatabase db = new TypeDatabase();
            db.ParseCorlib();

            var platform = new Platform();

            var decompiler = new Decompiler<x86_64AsmEntry>(db, platform, new x86_64CodeGenerator());
            //decompiler.Decompile(typeof(TypeMapAttribute).Assembly);
            decompiler.DecompileDB();
            decompiler.Decompile(assembly);

            var compiled = decompiler.AsmCollection.CompileAll();
            var strTab = platform.CompileStringTable();

            int mthd_idx = 12;

            CodeTester tester = new CodeTester();
            var strTab_ptr = tester.AllocStrtab(strTab);

            var A_mthd_ptr = tester.AllocMethod(compiled[mthd_idx - 1].Name, (uint)compiled[mthd_idx - 1].Code.Data.Length);
            tester.Relocate(compiled[mthd_idx - 1], A_mthd_ptr, strTab_ptr);

            var mthd_ptr = tester.AllocMethod(compiled[mthd_idx].Name, (uint)compiled[mthd_idx].Code.Data.Length);
            tester.Relocate(compiled[mthd_idx], mthd_ptr, strTab_ptr);

            var argsB = new List<byte>();
            argsB.AddRange(BitConverter.GetBytes(5.0f));
            argsB.AddRange(BitConverter.GetBytes(0));
            argsB.AddRange(BitConverter.GetBytes(5.0));

            tester.SetupMinimalRuntime(argsB.ToArray(), 4096, 4096, mthd_ptr);
        }
    }
}
