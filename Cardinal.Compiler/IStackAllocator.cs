﻿using System.Reflection;

namespace Cardinal.Compiler
{
    public interface IStackAllocator
    {
        void LoadArgument(IAsmEntry dst, int argIdx);
        void StoreArgument(IAsmEntry dst, int argIdx);
        void LoadLocal(IAsmEntry dst, int argIdx);
        void StoreLocal(IAsmEntry dst, int argIdx);
    }
}
