﻿namespace Cardinal.Compiler
{
    public interface ICodeGenerator<T> where T : IAsmEntry
    {
        public void Generate(DecodedOpCode[] dec_ops, T ent);
    }
}
