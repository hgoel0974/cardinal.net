using System;
using System.Reflection;

namespace Cardinal.Compiler
{
    public static class TypeExtensions
    {
        public static MethodInfo[] GetAllMethods(this Type t)
        {
            return t.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
        }

        public static ConstructorInfo[] GetAllConstructors(this Type t)
        {
            return t.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
        }
    }
}
