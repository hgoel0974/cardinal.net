﻿using Iced.Intel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Platforms.x86_64
{
    class StackAllocator : IStackAllocator
    {
        //Handles stack space allocation
        //Allocates space at the top of the stack for locals
        //Adds slots for spilled registers
        //Adds slots for backing up function call arguments
        //rsp tracks stack
        //rbp tracks top of stack for this frame
        //spilled registers get put into a cpu tracked stack
        //arguments are put above rbp
        //return values in rax

        public void LoadLocal(IAsmEntry dst, int argIdx)
        {
            var bod = dst.ImplMethod.GetMethodBody();
            var loc = bod.LocalVariables[argIdx].LocalType;
            var a = (dst as x86_64AsmEntry).Assembler;
            a.mov(rax, rdi + argIdx * 8);

            if (new Type[] { typeof(uint), typeof(int), typeof(short), typeof(ushort), typeof(byte), typeof(sbyte) }.Contains(loc))
            {
                dst.Operands.Push(OperandStackType.Int32);
                a.push(rax);
            }
            else if (new Type[] { typeof(ulong), typeof(long) }.Contains(loc))
            {
                dst.Operands.Push(OperandStackType.NativeInt);
                a.push(rax);
            }
            else if (new Type[] { typeof(float) }.Contains(loc))
            {
                dst.Operands.Push(OperandStackType.Float);
                a.push(rax);
            }
            else if (new Type[] { typeof(double) }.Contains(loc))
            {
                dst.Operands.Push(OperandStackType.Double);
                a.push(rax);
            }
            else
            {
                dst.Operands.Push(OperandStackType.Pointer);
                a.push(rax);
            }
        }

        public void StoreLocal(IAsmEntry dst, int argIdx)
        {
            var bod = dst.ImplMethod.GetMethodBody();
            var loc = bod.LocalVariables[argIdx].LocalType;
            var a = (dst as x86_64AsmEntry).Assembler;

            var e = dst.Operands.Pop();
            if (loc == typeof(float) && e == OperandStackType.Double)
            {
                //Round
                a.cvtsd2ss(xmm0, rsp + 0);
                a.movss(rsp + 0, xmm0);
            }
            a.pop(rax);
            if (loc == typeof(uint) && e != OperandStackType.Int32)
            {
                a.and(rax, unchecked((int)0xffffffff));
            }
            if (loc == typeof(ushort))
            {
                a.and(rax, unchecked(0x0000ffff));
            }
            if (loc == typeof(byte))
            {
                a.and(rax, unchecked(0x000000ff));
            }
            //TODO: Handle sign extension for signed types

            a.mov(rdi + argIdx * 8, rax);
        }

        public void LoadArgument(IAsmEntry dst, int argIdx)
        {
            var parms = dst.ImplMethod.GetParameters();
            Type loc = null;
            if (!dst.ImplMethod.IsStatic && argIdx == 0)
                loc = typeof(object);
            else if (!dst.ImplMethod.IsStatic)
                loc = parms[argIdx - 1].ParameterType;
            else
                loc = parms[argIdx].ParameterType;
            var a = (dst as x86_64AsmEntry).Assembler;
            a.mov(rax, rsi + argIdx * 8);

            if (new Type[] { typeof(uint), typeof(int), typeof(short), typeof(ushort), typeof(byte), typeof(sbyte) }.Contains(loc))
            {
                dst.Operands.Push(OperandStackType.Int32);
                a.push(rax);
            }
            else if (new Type[] { typeof(ulong), typeof(long) }.Contains(loc))
            {
                dst.Operands.Push(OperandStackType.NativeInt);
                a.push(rax);
            }
            else if (new Type[] { typeof(float) }.Contains(loc))
            {
                dst.Operands.Push(OperandStackType.Float);
                a.push(rax);
            }
            else if (new Type[] { typeof(double) }.Contains(loc))
            {
                dst.Operands.Push(OperandStackType.Double);
                a.push(rax);
            }
            else
            {
                dst.Operands.Push(OperandStackType.Pointer);
                a.push(rax);
            }
        }

        public void StoreArgument(IAsmEntry dst, int argIdx)
        {
            var parms = dst.ImplMethod.GetParameters();
            Type loc = null;
            if (!dst.ImplMethod.IsStatic && argIdx == 0)
                loc = typeof(object);
            else if (!dst.ImplMethod.IsStatic)
                loc = parms[argIdx - 1].ParameterType;
            else
                loc = parms[argIdx].ParameterType;
            var a = (dst as x86_64AsmEntry).Assembler;

            var e = dst.Operands.Pop();
            if (loc == typeof(float) && e == OperandStackType.Double)
            {
                //Round
                a.cvtsd2ss(xmm0, rsp + 0);
                a.movss(rsp + 0, xmm0);
            }
            a.pop(rax);
            if (loc == typeof(uint) && e != OperandStackType.Int32)
            {
                a.and(rax, unchecked((int)0xffffffff));
            }
            if (loc == typeof(ushort))
            {
                a.and(rax, unchecked(0x0000ffff));
            }
            if (loc == typeof(byte))
            {
                a.and(rax, unchecked(0x000000ff));
            }
            //TODO: Handle sign extension for signed types

            a.mov(rsi + argIdx * 8, rax);
        }
    }
}
