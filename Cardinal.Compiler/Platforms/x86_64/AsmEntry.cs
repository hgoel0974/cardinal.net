﻿using Cardinal.Compiler.Generators;
using Iced.Intel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Cardinal.Compiler.Platforms.x86_64
{
    public class CallInfo
    {
        public MethodBase Target;
    }

    public class x86_64AsmEntry : IAsmEntry
    {
        public IStackAllocator StackAlloc { get; set; }
        public IAsmPlatform Platform { get; set; }
        public MethodBase ImplMethod { get; set; }
        public MethodBase OgMethod { get; set; }
        public Assembler Assembler { get; }
        public Dictionary<int, Label> OpLabels { get; }
        public Dictionary<int, CallInfo> CallInstructions { get; }
        public List<int> StringInstructions { get; }
        public Stack<OperandStackType> Operands { get; set; }

        public x86_64AsmEntry()
        {
            Assembler = new Assembler(64);
            OpLabels = new Dictionary<int, Label>();
            CallInstructions = new Dictionary<int, CallInfo>();
            StringInstructions = new List<int>();
            Operands = new Stack<OperandStackType>();
        }

        public void BuildLabel(int instr_off)
        {
            OpLabels[instr_off] = Assembler.CreateLabel();
        }

        public void GetLabel(int instr_off, out Label lbl)
        {
            lbl = OpLabels[instr_off];
        }

        public RelocatedFunction Compile()
        {
            MemoryStream stream = new MemoryStream();
            var compiled = Assembler.Assemble(new StreamCodeWriter(stream), 0, BlockEncoderOptions.ReturnNewInstructionOffsets | BlockEncoderOptions.ReturnConstantOffsets);

            var relocOffs = new uint[CallInstructions.Count];
            var immOffs = new byte[CallInstructions.Count];
            var immSizes = new byte[CallInstructions.Count];
            var callNames = new string[CallInstructions.Count];

            var strOffs = new uint[StringInstructions.Count];
            var strInstrOffs = new uint[StringInstructions.Count];
            var strInstrSz = new uint[StringInstructions.Count];

            for (int i = 0; i < StringInstructions.Count; i++)
            {
                var strIntr = StringInstructions[i];

                strOffs[i] = compiled.Result[0].NewInstructionOffsets[strIntr];
                strInstrOffs[i] = compiled.Result[0].ConstantOffsets[strIntr].ImmediateOffset;
                strInstrSz[i] = compiled.Result[0].ConstantOffsets[strIntr].ImmediateSize;
            }

            for (int i = 0; i < CallInstructions.Count; i++)
            {
                var kvp = CallInstructions.ElementAt(i);

                relocOffs[i] = compiled.Result[0].NewInstructionOffsets[kvp.Key];
                immOffs[i] = compiled.Result[0].ConstantOffsets[kvp.Key].ImmediateOffset;
                immSizes[i] = compiled.Result[0].ConstantOffsets[kvp.Key].ImmediateSize;
                callNames[i] = AsmCollection<x86_64AsmEntry>.MangleName(kvp.Value.Target);
            }

            var relFunc = new RelocatedFunction()
            {
                Data = stream.GetBuffer(),
                InstructionOffsets = relocOffs,
                ImmediateOffsets = immOffs,
                ImmediateSizes = immSizes,
                CallNames = callNames,

                StringOffsets = strOffs,
                StringInstrOffsets = strInstrOffs,
                StringInstrSizes = strInstrSz,
            };

            return relFunc;
        }
    }
}
