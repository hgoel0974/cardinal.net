﻿using Iced.Intel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cardinal.Compiler.Platforms.x86_64
{
    class RegisterMapper
    {
        public static AssemblerRegister64 Map(int reg) => reg switch
        {
            0 => AssemblerRegisters.rax,
            1 => AssemblerRegisters.rbx,
            2 => AssemblerRegisters.rcx,
            3 => AssemblerRegisters.rdx,
            4 => AssemblerRegisters.rsi,
            5 => AssemblerRegisters.rdi,
            6 => AssemblerRegisters.r8,
            7 => AssemblerRegisters.r9,
            8 => AssemblerRegisters.r10,
            9 => AssemblerRegisters.r11,
            10 => AssemblerRegisters.r12,
            11 => AssemblerRegisters.r13,
            12 => AssemblerRegisters.r14,
            13 => AssemblerRegisters.r15,
            14 => AssemblerRegisters.rsp,
            15 => AssemblerRegisters.rbp,
            _ => throw new Exception("Unrecognized register.")
        };
    }
}
