﻿using Cardinal.Compiler.Tables;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Cardinal.Compiler.Platforms.x86_64
{
    class Platform : IAsmPlatform
    {
        StringTable StringTable;

        public Platform()
        {
            StringTable = new StringTable();
        }

        public IStackAllocator CreateStackAlloc(MethodBase og, MethodBase mthd)
        {
            var stackAlloc = new StackAllocator();
            return stackAlloc;
        }

        public int AddString(string s)
        {
            return StringTable.AddString(s);
        }

        public byte[] CompileStringTable()
        {
            return StringTable.Strings.ToArray();
        }
    }
}
