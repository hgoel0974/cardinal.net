﻿using Cardinal.Compiler.Generators;
using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Text;

namespace Cardinal.Compiler.Platforms.x86_64
{
    public class x86_64CodeGenerator : ICodeGenerator<x86_64AsmEntry>
    {
        private readonly Dictionary<OpCode, IAsmGenerator<x86_64AsmEntry>> generators;

        public x86_64CodeGenerator()
        {
            generators = new Dictionary<OpCode, IAsmGenerator<x86_64AsmEntry>>
            {
                [OpCodes.Ldarg_0] = new LdArg(0),
                [OpCodes.Ldarg_1] = new LdArg(1),
                [OpCodes.Ldarg_2] = new LdArg(2),
                [OpCodes.Ldarg_3] = new LdArg(3),
                [OpCodes.Ldarg_S] = new LdArg(isShort: true),
                [OpCodes.Ldarg] = new LdArg(isShort: false),

                [OpCodes.Ldloc_0] = new LdLoc(0),
                [OpCodes.Ldloc_1] = new LdLoc(1),
                [OpCodes.Ldloc_2] = new LdLoc(2),
                [OpCodes.Ldloc_3] = new LdLoc(3),
                [OpCodes.Ldloc_S] = new LdLoc(isShort: true),
                [OpCodes.Ldloc] = new LdLoc(isShort: false),

                [OpCodes.Stloc_0] = new StLoc(0),
                [OpCodes.Stloc_1] = new StLoc(1),
                [OpCodes.Stloc_2] = new StLoc(2),
                [OpCodes.Stloc_3] = new StLoc(3),
                [OpCodes.Stloc_S] = new StLoc(isShort: true),
                [OpCodes.Stloc] = new StLoc(isShort: false),

                [OpCodes.Ldc_I4_0] = new Ldc(0),
                [OpCodes.Ldc_I4_1] = new Ldc(1),
                [OpCodes.Ldc_I4_2] = new Ldc(2),
                [OpCodes.Ldc_I4_3] = new Ldc(3),
                [OpCodes.Ldc_I4_4] = new Ldc(4),
                [OpCodes.Ldc_I4_5] = new Ldc(5),
                [OpCodes.Ldc_I4_6] = new Ldc(6),
                [OpCodes.Ldc_I4_7] = new Ldc(7),
                [OpCodes.Ldc_I4_8] = new Ldc(8),
                [OpCodes.Ldc_I4_M1] = new Ldc(-1),
                [OpCodes.Ldc_I4_S] = new Ldc(-2, 1),
                [OpCodes.Ldc_I4] = new Ldc(-2, 4),
                [OpCodes.Ldc_I8] = new Ldc(-2, 8),
                [OpCodes.Ldc_R4] = new Ldc(-2, 5),
                [OpCodes.Ldc_R8] = new Ldc(-2, 9),

                [OpCodes.Ldnull] = new Ldnull(),
                [OpCodes.Ldstr] = new LdStr(),

                [OpCodes.Conv_R8] = new Conv(ConvTypes.R8),

                [OpCodes.Brfalse] = new Brfalse(isShort: false),
                [OpCodes.Brfalse_S] = new Brfalse(isShort: true),

                [OpCodes.Brtrue] = new Brtrue(isShort: false),
                [OpCodes.Brtrue_S] = new Brtrue(isShort: true),

                [OpCodes.Beq] = new Beq(isShort: false),
                [OpCodes.Beq_S] = new Beq(isShort: true),

                [OpCodes.Bne_Un] = new Bne(isShort: false, isUnsigned: true),
                [OpCodes.Bne_Un_S] = new Bne(isShort: true, isUnsigned: true),

                [OpCodes.Bge] = new Bge(isShort: false, isUnsigned: false),
                [OpCodes.Bge_S] = new Bge(isShort: true, isUnsigned: false),
                [OpCodes.Bge_Un] = new Bge(isShort: false, isUnsigned: true),
                [OpCodes.Bge_Un_S] = new Bge(isShort: true, isUnsigned: true),

                [OpCodes.Bgt] = new Bgt(isShort: false, isUnsigned: false),
                [OpCodes.Bgt_S] = new Bgt(isShort: true, isUnsigned: false),
                [OpCodes.Bgt_Un] = new Bgt(isShort: false, isUnsigned: true),
                [OpCodes.Bgt_Un_S] = new Bgt(isShort: true, isUnsigned: true),

                [OpCodes.Ble] = new Bge(isShort: false, isUnsigned: false),
                [OpCodes.Ble_S] = new Bge(isShort: true, isUnsigned: false),
                [OpCodes.Ble_Un] = new Bge(isShort: false, isUnsigned: true),
                [OpCodes.Ble_Un_S] = new Bge(isShort: true, isUnsigned: true),

                [OpCodes.Blt] = new Bgt(isShort: false, isUnsigned: false),
                [OpCodes.Blt_S] = new Bgt(isShort: true, isUnsigned: false),
                [OpCodes.Blt_Un] = new Bgt(isShort: false, isUnsigned: true),
                [OpCodes.Blt_Un_S] = new Bgt(isShort: true, isUnsigned: true),

                [OpCodes.Br] = new Br(isShort: false),
                [OpCodes.Br_S] = new Br(isShort: true),

                [OpCodes.Call] = new Call(),
                [OpCodes.Callvirt] = new CallVirt(),

                [OpCodes.Add] = new Add(overflowCheck: false, signed: true),

                [OpCodes.Sub] = new Sub(overflowCheck: false, signed: true),

                [OpCodes.Mul] = new Mul(overflowCheck: false, signed: true),

                [OpCodes.Div] = new Div(overflowCheck: false, signed: true),

                [OpCodes.Neg] = new Neg(),
                [OpCodes.And] = new And(),
                [OpCodes.Xor] = new Xor(),
                [OpCodes.Or] = new Or(),
                [OpCodes.Not] = new Not(),

                [OpCodes.Shl] = new Shl(),
                [OpCodes.Shr] = new Shr(isSigned: true),
                [OpCodes.Shr_Un] = new Shr(isSigned: false),

                [OpCodes.Ceq] = new Ceq(),

                [OpCodes.Clt] = new Clt(isSigned: true),
                [OpCodes.Clt_Un] = new Clt(isSigned: false),

                [OpCodes.Cgt] = new Cgt(isSigned: true),
                [OpCodes.Cgt_Un] = new Cgt(isSigned: false),

                [OpCodes.Arglist] = new Arglist(),
                [OpCodes.Pop] = new Pop(),
                [OpCodes.Newobj] = new Newobj(),
                [OpCodes.Ret] = new Ret(),
                [OpCodes.Nop] = new Nop(),
            };
        }

        public void Generate(DecodedOpCode[] dec_ops, x86_64AsmEntry ent)
        {
            for (int i = 0; i < dec_ops.Length; i++)
                ent.BuildLabel(dec_ops[i].InstrOffset);

            for (int i = 0; i < dec_ops.Length; i++)
            {
                if (generators.ContainsKey(dec_ops[i].Op))
                {
                    ent.GetLabel(dec_ops[i].InstrOffset, out var lbl);
                    ent.Assembler.Label(ref lbl);
                    generators[dec_ops[i].Op].Generate(ent, dec_ops[i]);
                }
                else
                {
                    //Emit a nop for unimplemented instructions to maintain the assembler's one label per instruction requirement
                    ent.GetLabel(dec_ops[i].InstrOffset, out var lbl);
                    ent.Assembler.Label(ref lbl);
                    ent.Assembler.nop();

                    Console.WriteLine($"[Warning] Unimplemented instruction: {dec_ops[i].Op.Name}");
                }
            }
        }
    }
}
