using System.Reflection;
using System;
using System.Collections.Generic;
using Iced.Intel;
using System.Linq;

namespace Cardinal.Compiler
{
    public struct RelocatedFunction
    {
        public byte[] Data;
        public uint[] InstructionOffsets;
        public byte[] ImmediateOffsets;
        public byte[] ImmediateSizes;
        public string[] CallNames;
        internal uint[] StringOffsets;
        internal uint[] StringInstrOffsets;
        internal uint[] StringInstrSizes;
    }

    public enum OperandStackType
    {
        Unknown,
        Int32,
        NativeInt,
        Float,
        Double,
        Pointer,
    }

    public interface IAsmEntry
    {
        IStackAllocator StackAlloc { get; set; }
        IAsmPlatform Platform { get; set; }
        MethodBase ImplMethod { get; set; }
        MethodBase OgMethod { get; set; }
        Stack<OperandStackType> Operands { get; set; }
        RelocatedFunction Compile();
    }

    public struct CompiledFunction
    {
        public string Name { get; set; }
        public RelocatedFunction Code { get; set; }
    }

    public class AsmCollection<T> where T : IAsmEntry, new()
    {
        private readonly Dictionary<string, T> Functions;
        private IAsmPlatform platform;

        public AsmCollection(IAsmPlatform platform)
        {
            Functions = new Dictionary<string, T>();
            this.platform = platform;
        }

        public static string MangleName(MethodBase og)
        {
            var parms = og.GetParameters();
            var str = "";
            foreach (var p in parms)
            {
                str += p.ParameterType.FullName + "_";
                if (p.IsIn)
                    str += "I";
                if (p.IsOut)
                    str += "O";
                if (p.IsRetval)
                    str += "R";
                str += "_";
            }
            return (og.DeclaringType.FullName + "." + og.Name + "_Parms_" + str).Replace('.', '_');
        }

        public bool FunctionExists(MethodBase og)
        {
            var fname = MangleName(og);
            return (Functions.ContainsKey(fname));
        }

        public void CreateFunction(MethodBase og, MethodBase mthd)
        {
            var fname = MangleName(og);

            if (Functions.ContainsKey(fname))
                throw new Exception("Function exists already.");

            Functions[fname] = new T()
            {
                StackAlloc = platform.CreateStackAlloc(og, mthd),
                ImplMethod = mthd,
                OgMethod = og,
                Platform = platform,
            };
        }

        public CompiledFunction[] CompileAll()
        {
            var funcs = new CompiledFunction[Functions.Count];
            for (int i = 0; i < funcs.Length; i++)
            {
                funcs[i] = new CompiledFunction()
                {
                    Name = Functions.ElementAt(i).Key,
                    Code = Functions.ElementAt(i).Value.Compile(),
                };
            }
            return funcs;
        }

        public T GetFunction(MethodBase og)
        {
            return GetFunction(MangleName(og));
        }

        public T GetFunction(string fname)
        {
            return Functions[fname];
        }

        public void EndFunction(MethodBase og)
        {

        }
    }
}
