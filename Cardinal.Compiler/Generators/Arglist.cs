﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Arglist : IAsmGenerator<x86_64AsmEntry>
    {
        public Arglist()
        {
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            entry.Operands.Push(OperandStackType.NativeInt);
            entry.Assembler.push(rsi);
        }
    }
}
