﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Ldc : IAsmGenerator<x86_64AsmEntry>
    {
        int fixedIndex = -2;
        int type_sz;

        public Ldc(int fixed_arg_idx, int sz = 0)
        {
            type_sz = sz;
            fixedIndex = fixed_arg_idx;
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            if (fixedIndex != -2)    //Is fixed index ldarg instruction
            {
                entry.Assembler.push(fixedIndex);
                entry.Operands.Push(OperandStackType.Int32);
            }
            else
            {
                //Is variable index ldarg
                if (type_sz == 1)
                {
                    entry.Assembler.push(opc.I8);
                    entry.Operands.Push(OperandStackType.Int32);
                }
                else if(type_sz == 4)
                {
                    entry.Assembler.push(opc.I32);
                    entry.Operands.Push(OperandStackType.Int32);
                }
                else if (type_sz == 8)
                {
                    entry.Assembler.mov(rax, opc.I64);
                    entry.Assembler.push(rax);
                    entry.Operands.Push(OperandStackType.NativeInt);
                }
                else if (type_sz == 5)
                {
                    entry.Assembler.push(BitConverter.SingleToInt32Bits(opc.R32));
                    entry.Operands.Push(OperandStackType.Float);
                }
                else if (type_sz == 9)
                {
                    entry.Assembler.mov(rax, BitConverter.DoubleToInt64Bits(opc.R64));
                    entry.Assembler.push(rax);
                    entry.Operands.Push(OperandStackType.Double);
                }
            }
        }
    }
}
