﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Xor : IAsmGenerator<x86_64AsmEntry>
    {
        public Xor()
        {
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            var a = entry.Operands.Pop();
            var b = entry.Operands.Pop();

            if (a == OperandStackType.NativeInt | b == OperandStackType.NativeInt)
                entry.Operands.Push(OperandStackType.NativeInt);
            else if (a == OperandStackType.Pointer | b == OperandStackType.Pointer)
                entry.Operands.Push(OperandStackType.Pointer);
            else
                entry.Operands.Push(OperandStackType.Int32);

            entry.Assembler.pop(rax);
            entry.Assembler.pop(rbx);
            entry.Assembler.xor(rax, rbx);
            entry.Assembler.push(rax);
        }
    }
}
