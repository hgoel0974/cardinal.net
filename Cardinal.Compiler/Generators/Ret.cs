﻿using Cardinal.Compiler.Platforms.x86_64;
using Iced.Intel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Ret : IAsmGenerator<x86_64AsmEntry>
    {
        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            //Arguments are already on the stack by design

            if (entry.ImplMethod is MethodInfo)
            {
                var mI = entry.ImplMethod as MethodInfo;
                if (mI.ReturnType != typeof(void))
                {
                    entry.Operands.Pop();
                    entry.Assembler.pop(rax);
                }
            }
            else if (entry.ImplMethod is ConstructorInfo)
            {
                //Returns 'this'
                entry.Operands.Pop();
                entry.Assembler.pop(rax);
            }
            entry.Assembler.ret();
        }
    }
}
