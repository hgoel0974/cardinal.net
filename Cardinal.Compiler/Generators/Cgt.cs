﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Cgt : IAsmGenerator<x86_64AsmEntry>
    {
        private bool signed;
        public Cgt(bool isSigned)
        {
            signed = isSigned;
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            var a = entry.Operands.Pop();
            var b = entry.Operands.Pop();
            entry.Operands.Push(OperandStackType.Int32);

            entry.Assembler.pop(rax);
            entry.Assembler.pop(rbx);
            entry.Assembler.cmp(rax, rbx);

            if (signed)
                entry.Assembler.setl(al);
            else
                entry.Assembler.setb(al);
            entry.Assembler.push(rax);
        }
    }
}
