﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Brfalse : IAsmGenerator<x86_64AsmEntry>
    {
        bool isShortVar = false;

        public Brfalse(bool isShort)
        {
            isShortVar = isShort;
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            var a = entry.Operands.Pop();

            entry.Assembler.pop(rax);
            entry.Assembler.cmp(rax, 0);
            entry.GetLabel(opc.BrTargets[0], out var lbl);
            entry.Assembler.je(lbl);
        }
    }
}
