﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Not : IAsmGenerator<x86_64AsmEntry>
    {
        public Not()
        {
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            var a = entry.Operands.Pop();
            entry.Operands.Push(a);
            
            entry.Assembler.pop(rax);
            entry.Assembler.not(rax);
            entry.Assembler.push(rax);
        }
    }
}
