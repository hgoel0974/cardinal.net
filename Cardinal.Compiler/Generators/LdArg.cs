﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cardinal.Compiler.Generators
{
    internal class LdArg : IAsmGenerator<x86_64AsmEntry>
    {
        int fixedIndex = -1;
        bool isShortVar = false;

        public LdArg(int fixed_arg_idx)
        {
            fixedIndex = fixed_arg_idx;
        }

        public LdArg(bool isShort)
        {
            isShortVar = isShort;
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            if(fixedIndex != -1)    //Is fixed index ldarg instruction
            {
                entry.StackAlloc.LoadArgument(entry, fixedIndex);
            }
            else
            {
                //Is variable index ldarg
                if (isShortVar)
                {
                    entry.StackAlloc.LoadArgument(entry, opc.I8);
                }
                else
                {
                    entry.StackAlloc.LoadArgument(entry, (ushort)opc.I16);
                }
            }
        }
    }
}
