﻿using Cardinal.Compiler.Platforms.x86_64;
using Iced.Intel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Call : IAsmGenerator<x86_64AsmEntry>
    {
        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            //Arguments are already on the stack by design
            //Pop and load them into RSI
            var parms = entry.ImplMethod.GetParameters();
            var parmsSpace = parms.Length * 8;
            var localsSpace = entry.ImplMethod.GetMethodBody().LocalVariables.Count * 8;
            entry.Assembler.add(rsi, parmsSpace);

            for (int i = 0; i < parms.Length; i++)
            {
                entry.Operands.Pop();
                entry.Assembler.pop(rax);
                entry.Assembler.mov(rsi + (8 * (parms.Length - 1 - i)), rax);
            }

            entry.Assembler.add(rdi, localsSpace);

            //make function call
            entry.CallInstructions[entry.Assembler.Instructions.Count] = new CallInfo()
            {
                Target = opc.Method
            };
            entry.Assembler.call(0);

            if (opc.Method is ConstructorInfo)
            {
                entry.Operands.Push(OperandStackType.Pointer);
                entry.Assembler.push(rax);
            }
            else if (opc.Method is MethodInfo)
            {
                var m = opc.Method as MethodInfo;
                if (m.ReturnType != typeof(void))
                {
                    if (new Type[] { typeof(uint), typeof(int), typeof(short), typeof(ushort), typeof(byte), typeof(sbyte) }.Contains(m.ReturnType))
                    {
                        entry.Operands.Push(OperandStackType.Int32);
                        entry.Assembler.push(rax);
                    }
                    else if (new Type[] { typeof(ulong), typeof(long) }.Contains(m.ReturnType))
                    {
                        entry.Operands.Push(OperandStackType.NativeInt);
                        entry.Assembler.push(rax);
                    }
                    else if (new Type[] { typeof(float) }.Contains(m.ReturnType))
                    {
                        entry.Operands.Push(OperandStackType.Float);
                        entry.Assembler.push(rax);
                    }
                    else if (new Type[] { typeof(double) }.Contains(m.ReturnType))
                    {
                        entry.Operands.Push(OperandStackType.Double);
                        entry.Assembler.push(rax);
                    }
                    else
                    {
                        entry.Operands.Push(OperandStackType.Pointer);
                        entry.Assembler.push(rax);
                    }
                }
            }

            //Unload arguments and restore locals base
            entry.Assembler.sub(rdi, localsSpace);
            entry.Assembler.sub(rsi, parmsSpace);
        }
    }
}
