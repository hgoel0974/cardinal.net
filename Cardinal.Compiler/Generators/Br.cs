﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Br : IAsmGenerator<x86_64AsmEntry>
    {
        bool isShortVar = false;

        public Br(bool isShort)
        {
            isShortVar = isShort;
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            entry.GetLabel(opc.BrTargets[0], out var lbl);
            entry.Assembler.jmp(lbl);
        }
    }
}
