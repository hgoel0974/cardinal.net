﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cardinal.Compiler.Generators
{
    class LdLoc : IAsmGenerator<x86_64AsmEntry>
    {
        int fixedIndex = -1;
        bool isShortVar = false;

        public LdLoc(int fixed_arg_idx)
        {
            fixedIndex = fixed_arg_idx;
        }

        public LdLoc(bool isShort)
        {
            isShortVar = isShort;
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            if (fixedIndex != -1)    //Is fixed index ldarg instruction
            {
                entry.StackAlloc.LoadLocal(entry, fixedIndex);
            }
            else
            {
                //Is variable index ldarg
                if (isShortVar)
                {
                    entry.StackAlloc.LoadLocal(entry, opc.I8);
                }
                else
                {
                    entry.StackAlloc.LoadLocal(entry, (ushort)opc.I16);
                }
            }
        }
    }
}
