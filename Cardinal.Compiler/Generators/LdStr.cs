﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    internal class LdStr : IAsmGenerator<x86_64AsmEntry>
    {
        public LdStr()
        {
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            entry.Operands.Push(OperandStackType.Pointer);

            var strTabOff = entry.Platform.AddString(opc.Str);
            entry.StringInstructions.Add(entry.Assembler.Instructions.Count);
            entry.Assembler.push(strTabOff);
        }
    }
}
