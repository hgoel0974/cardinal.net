﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Ceq : IAsmGenerator<x86_64AsmEntry>
    {
        public Ceq()
        {
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            var a = entry.Operands.Pop();
            var b = entry.Operands.Pop();
            entry.Operands.Push(OperandStackType.Int32);

            entry.Assembler.pop(rax);
            entry.Assembler.pop(rbx);
            entry.Assembler.cmp(rax, rbx);
            entry.Assembler.sete(al);
            entry.Assembler.push(rax);
        }
    }
}
