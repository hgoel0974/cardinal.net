﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Div : IAsmGenerator<x86_64AsmEntry>
    {
        bool checkOverflow = false;
        bool doSigned = false;

        public Div(bool overflowCheck, bool signed)
        {
            checkOverflow = overflowCheck;
            doSigned = signed;
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            var a_type = entry.Operands.Pop();
            var b_type = entry.Operands.Pop();

            if (a_type == OperandStackType.Int32)
            {
                if (b_type == OperandStackType.Int32)
                {
                    entry.Assembler.xor(rdx, rdx);
                    entry.Assembler.pop(rbx);
                    entry.Assembler.pop(rax);
                    if (doSigned)
                        entry.Assembler.idiv(rbx);
                    else
                        throw new NotImplementedException();
                    entry.Assembler.push(rax);

                    entry.Operands.Push(OperandStackType.Int32);
                }
                else if (b_type == OperandStackType.NativeInt)
                {
                    entry.Assembler.xor(rdx, rdx);
                    entry.Assembler.pop(rbx);
                    entry.Assembler.pop(rax);
                    if (doSigned)
                        entry.Assembler.idiv(rbx);
                    else
                        throw new NotImplementedException();
                    entry.Assembler.push(rax);

                    entry.Operands.Push(OperandStackType.NativeInt);
                }
                else if (b_type == OperandStackType.Pointer)
                {
                    entry.Assembler.xor(rdx, rdx);
                    entry.Assembler.pop(rbx);
                    entry.Assembler.pop(rax);
                    if (doSigned)
                        entry.Assembler.idiv(rbx);
                    else
                        throw new NotImplementedException();
                    entry.Assembler.push(rax);

                    entry.Operands.Push(OperandStackType.Pointer);
                }
            }
            else if (a_type == OperandStackType.NativeInt)
            {
                if (b_type == OperandStackType.Int32)
                {
                    entry.Assembler.xor(rdx, rdx);
                    entry.Assembler.pop(rbx);
                    entry.Assembler.pop(rax);
                    if (doSigned)
                        entry.Assembler.idiv(rbx);
                    else
                        throw new NotImplementedException();
                    entry.Assembler.push(rax);

                    entry.Operands.Push(OperandStackType.NativeInt);
                }
                else if (b_type == OperandStackType.NativeInt)
                {
                    entry.Assembler.xor(rdx, rdx);
                    entry.Assembler.pop(rbx);
                    entry.Assembler.pop(rax);
                    if (doSigned)
                        entry.Assembler.idiv(rbx);
                    else
                        throw new NotImplementedException();
                    entry.Assembler.push(rax);

                    entry.Operands.Push(OperandStackType.NativeInt);
                }
                else if (b_type == OperandStackType.Pointer)
                {
                    entry.Assembler.xor(rdx, rdx);
                    entry.Assembler.pop(rbx);
                    entry.Assembler.pop(rax);
                    if (doSigned)
                        entry.Assembler.idiv(rbx);
                    else
                        throw new NotImplementedException();
                    entry.Assembler.push(rax);

                    entry.Operands.Push(OperandStackType.Pointer);
                }
            }
            else if (a_type == OperandStackType.Float)
            {
                if (b_type == OperandStackType.Float)
                {
                    entry.Assembler.movss(xmm0, __[rsp + 8]);
                    entry.Assembler.divss(xmm0, __[rsp]);
                    entry.Assembler.add(rsp, 8);
                    entry.Assembler.movss(__[rsp], xmm0);

                    entry.Operands.Push(OperandStackType.Float);
                }
                else throw new NotImplementedException();
            }
            else if (a_type == OperandStackType.Double)
            {
                if (b_type == OperandStackType.Double)
                {
                    entry.Assembler.movsd(xmm0, __[rsp + 8]);
                    entry.Assembler.divsd(xmm0, __[rsp]);
                    entry.Assembler.add(rsp, 8);
                    entry.Assembler.movsd(__[rsp], xmm0);

                    entry.Operands.Push(OperandStackType.Double);
                }
                else throw new NotImplementedException();
            }
            else if (a_type == OperandStackType.Pointer)
            {
                if (b_type == OperandStackType.Int32)
                {
                    entry.Assembler.xor(rdx, rdx);
                    entry.Assembler.pop(rbx);
                    entry.Assembler.pop(rax);
                    if (doSigned)
                        entry.Assembler.idiv(rbx);
                    else
                        throw new NotImplementedException();
                    entry.Assembler.push(rax);

                    entry.Operands.Push(OperandStackType.Pointer);
                }
                else if (b_type == OperandStackType.NativeInt)
                {
                    entry.Assembler.xor(rdx, rdx);
                    entry.Assembler.pop(rbx);
                    entry.Assembler.pop(rax);
                    if (doSigned)
                        entry.Assembler.idiv(rbx);
                    else
                        throw new NotImplementedException();
                    entry.Assembler.push(rax);

                    entry.Operands.Push(OperandStackType.Pointer);
                }
            }
        }
    }
}
