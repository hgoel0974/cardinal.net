﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Neg : IAsmGenerator<x86_64AsmEntry>
    {
        public Neg()
        {
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            var a = entry.Operands.Pop();
            entry.Operands.Push(a);

            if (a == OperandStackType.Int32 | a == OperandStackType.NativeInt)
            {
                entry.Assembler.pop(rax);
                entry.Assembler.neg(rax);
                entry.Assembler.push(rax);
            }
            else if (a == OperandStackType.Float)
            {
                entry.Assembler.pop(rax);
                entry.Assembler.mov(rbx, 0x80000000);
                entry.Assembler.xor(rax, rbx);
                entry.Assembler.push(rax);
            }
            else if (a == OperandStackType.Double)
            {
                entry.Assembler.pop(rax);
                entry.Assembler.mov(rbx, 0x8000000000000000);
                entry.Assembler.xor(rax, rbx);
                entry.Assembler.push(rax);
            }
        }
    }
}
