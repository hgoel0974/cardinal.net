﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cardinal.Compiler.Generators
{
    class StLoc : IAsmGenerator<x86_64AsmEntry>
    {
        int fixedIndex = -1;
        bool isShortVar = false;

        public StLoc(int fixed_arg_idx)
        {
            fixedIndex = fixed_arg_idx;
        }

        public StLoc(bool isShort)
        {
            isShortVar = isShort;
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            if (fixedIndex != -1)
            {
                entry.StackAlloc.StoreLocal(entry, fixedIndex);
            }
            else
            {
                if (isShortVar)
                {
                    entry.StackAlloc.StoreLocal(entry, opc.I8);
                }
                else
                {
                    entry.StackAlloc.StoreLocal(entry, (ushort)opc.I16);
                }
            }
        }
    }
}
