﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Shl : IAsmGenerator<x86_64AsmEntry>
    {
        public Shl()
        {
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            var a = entry.Operands.Pop();
            var b = entry.Operands.Pop();

            entry.Operands.Push(b);
            
            entry.Assembler.pop(rbx);
            entry.Assembler.pop(rax);
            entry.Assembler.shl(rax, bl);
            entry.Assembler.push(rax);
        }
    }
}
