﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Ble : IAsmGenerator<x86_64AsmEntry>
    {
        bool isShortVar = false;
        bool isUnsigned = false;

        public Ble(bool isShort, bool isUnsigned)
        {
            isShortVar = isShort;
            this.isUnsigned = isUnsigned;
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            var a = entry.Operands.Pop();
            var b = entry.Operands.Pop();

            entry.Assembler.pop(rax);
            entry.Assembler.pop(rbx);
            entry.Assembler.cmp(rax, rbx);
            entry.GetLabel(opc.BrTargets[0], out var lbl);

            if (isUnsigned)
                entry.Assembler.jae(lbl);
            else
                entry.Assembler.jge(lbl);
        }
    }
}
