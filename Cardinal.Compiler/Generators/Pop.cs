﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    class Pop : IAsmGenerator<x86_64AsmEntry>
    {
        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            entry.Operands.Pop();
            entry.Assembler.pop(rax);
        }
    }
}
