﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cardinal.Compiler.Generators
{
    class Ldnull : IAsmGenerator<x86_64AsmEntry>
    {
        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            entry.Assembler.push(0);
            entry.Operands.Push(OperandStackType.Int32);
        }
    }
}
