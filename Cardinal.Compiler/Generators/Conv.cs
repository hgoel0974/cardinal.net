﻿using Cardinal.Compiler.Platforms.x86_64;
using System;
using System.Collections.Generic;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler.Generators
{
    enum ConvTypes
    {
        NativeInt,
        I1,
        I2,
        I4,
        I8,
        NativeUInt,
        U1,
        U2,
        U4,
        U8,
        R4,
        R8,

    }

    class Conv : IAsmGenerator<x86_64AsmEntry>
    {
        private ConvTypes type;
        public Conv(ConvTypes type)
        {
            this.type = type;
        }

        public void Generate(x86_64AsmEntry entry, DecodedOpCode opc)
        {
            var a = entry.Operands.Pop();

            switch (type)
            {
                case ConvTypes.R8:
                    if (a == OperandStackType.NativeInt)
                    {
                        entry.Assembler.cvtsi2sd(xmm0, __qword_ptr[rsp]);
                        entry.Assembler.movsd(__[rsp], xmm0);
                    }
                    else if (a == OperandStackType.Int32)
                    {
                        entry.Assembler.cvtsi2sd(xmm0, __dword_ptr[rsp]);
                        entry.Assembler.movsd(__[rsp], xmm0);
                    }
                    else if (a == OperandStackType.Float)
                    {
                        entry.Assembler.cvtss2sd(xmm0, __dword_ptr[rsp]);
                        entry.Assembler.movsd(__[rsp], xmm0);
                    }
                    entry.Operands.Push(OperandStackType.Double);
                    break;
            }
        }
    }
}
