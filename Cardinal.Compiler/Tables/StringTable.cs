﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cardinal.Compiler.Tables
{
    class StringTable
    {
        //large string array serialized as is
        //offsets to the table are updated as needed
        public List<byte> Strings { get; private set; }
        public Dictionary<string, int> Offsets { get; private set; }

        public StringTable()
        {
            Strings = new List<byte>();
            Offsets = new Dictionary<string, int>();
        }

        public int AddString(string s)
        {
            if (!Offsets.ContainsKey(s))
            {
                Offsets[s] = Strings.Count;
                Strings.AddRange(Encoding.ASCII.GetBytes(s));
                Strings.Add(0);
            }
            return Offsets[s];
        }
    }
}
