﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cardinal.Compiler.Tables
{
    public class BranchTable
    {
        //Store relocation instruction indices to later resolve into offsets for fixup
        List<int> InstructionIndices;

        public BranchTable() {
            InstructionIndices = new List<int>();
        }

        public void RegisterRelocation(int idx)
        {
            InstructionIndices.Add(idx);
        }
    }
}
