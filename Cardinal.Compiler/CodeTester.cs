﻿using Iced.Intel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using static Iced.Intel.AssemblerRegisters;

namespace Cardinal.Compiler
{
    class CodeTester
    {
        const uint PAGE_EXECUTE_READWRITE = 0x40;
        const uint MEM_COMMIT = 0x1000;

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern IntPtr VirtualAlloc(IntPtr lpAddress, uint dwSize, uint flAllocationType, uint flProtect);

        private delegate void IntReturner();
        private Dictionary<string, IntPtr> memRegions;
        private List<IntPtr> strtabs;
        private IntPtr argSpace;
        private IntPtr localsSpace;

        public CodeTester()
        {
            memRegions = new Dictionary<string, IntPtr>();
            strtabs = new List<IntPtr>();
        }

        public IntPtr AllocMethod(string name, uint sz)
        {
            IntPtr buf = VirtualAlloc(IntPtr.Zero, sz, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
            memRegions[name] = buf;
            return buf;
        }

        public IntPtr AllocStrtab(byte[] strs)
        {
            IntPtr buf = Marshal.AllocHGlobal(strs.Length);
            Marshal.Copy(strs, 0, buf, strs.Length);
            strtabs.Add(buf);
            return buf;
        }

        public void SetupMinimalRuntime(byte[] args, int args_sz, int locals_sz, IntPtr call)
        {
            //Generate assembly to setup rdi and rsi
            argSpace = Marshal.AllocHGlobal(args_sz);
            localsSpace = Marshal.AllocHGlobal(locals_sz);

            Marshal.Copy(args, 0, argSpace, args.Length);

            Assembler assem = new Assembler(64);
            assem.push(rsi);
            assem.push(rdi);
            assem.push(rax);
            assem.push(rbx);
            assem.mov(rsi, (long)argSpace);
            assem.mov(rdi, (long)localsSpace);
            assem.call((ulong)call);
            assem.pop(rbx);
            //assem.mov(rax, __[rsi]);
            assem.mov(__[rsi], rax);
            assem.pop(rax);
            assem.pop(rdi);
            assem.pop(rsi);
            assem.ret();

            var runtime_rip = AllocMethod("minimal_runtime", 1024);

            MemoryStream stream = new MemoryStream();
            assem.Assemble(new StreamCodeWriter(stream), (ulong)runtime_rip);
            var buf = stream.GetBuffer();
            Marshal.Copy(buf, 0, runtime_rip, buf.Length);

            IntReturner ptr = (IntReturner)Marshal.GetDelegateForFunctionPointer(runtime_rip, typeof(IntReturner));
            ptr();

            unsafe
            {
                var l_p = (double*)argSpace;
                Console.WriteLine(l_p[0]);
            }
        }

        public void Relocate(CompiledFunction func, IntPtr addr, IntPtr strtab)
        {
            unsafe
            {
                // You can also pass in a hex string, eg. "90 91 929394", or you can use your own CodeReader
                // reading data from a file or memory etc
                Marshal.Copy(func.Code.Data, 0, addr, func.Code.Data.Length);

                unsafe
                {
                    for (int i = 0; i < func.Code.InstructionOffsets.Length; i++)
                    {
                        uint instr_off = func.Code.InstructionOffsets[i];
                        var ptr_off = memRegions[func.Code.CallNames[i]];
                        var val = (long)ptr_off - (long)addr;

                        if (func.Code.ImmediateSizes[i] == 4)
                        {
                            int* ptr = (int*)((ulong)addr + instr_off + func.Code.ImmediateOffsets[i]);
                            ptr[0] += (int)val;
                        }
                    }

                    var codeBytes = new byte[func.Code.Data.Length];
                    Marshal.Copy(addr, codeBytes, 0, codeBytes.Length);
                    var codeReader = new ByteArrayCodeReader(codeBytes);
                    var decoder = Iced.Intel.Decoder.Create(64, codeReader);
                    decoder.IP = (ulong)addr;
                    ulong endRip = decoder.IP + (uint)codeBytes.Length;

                    // This list is faster than List<Instruction> since it uses refs to the Instructions
                    // instead of copying them (each Instruction is 32 bytes in size). It has a ref indexer,
                    // and a ref iterator. Add() uses 'in' (ref readonly).
                    var instructions = new InstructionList();
                    while (decoder.IP < endRip)
                    {
                        // The method allocates an uninitialized element at the end of the list and
                        // returns a reference to it which is initialized by Decode().
                        decoder.Decode(out instructions.AllocUninitializedElement());
                    }

                    // Formatters: Masm*, Nasm*, Gas* (AT&T) and Intel* (XED)
                    var formatter = new NasmFormatter();
                    formatter.Options.DigitSeparator = "`";
                    formatter.Options.FirstOperandCharIndex = 10;
                    var output = new StringOutput();
                    // Use InstructionList's ref iterator (C# 7.3) to prevent copying 32 bytes every iteration
                    foreach (ref var instr in instructions)
                    {
                        // Don't use instr.ToString(), it allocates more, uses masm syntax and default options
                        formatter.Format(instr, output);
                        Console.Write(instr.IP.ToString("X16"));
                        Console.Write(" ");
                        int instrLen = instr.Length;
                        int byteBaseIndex = (int)(instr.IP - (ulong)addr);
                        for (int i = 0; i < instrLen; i++)
                            Console.Write(codeBytes[byteBaseIndex + i].ToString("X2"));
                        int missingBytes = 10 - instrLen;
                        for (int i = 0; i < missingBytes; i++)
                            Console.Write("  ");
                        Console.Write(" ");
                        Console.WriteLine(output.ToStringAndReset());
                    }
                }

            }
        }
    }
}
