﻿namespace Cardinal.Compiler
{
    public interface IAsmGenerator<T> where T : IAsmEntry
    {
        void Generate(T entry, DecodedOpCode opc);
    }
}
