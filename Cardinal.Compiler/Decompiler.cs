using Cardinal.Compiler.Generators;
using Iced.Intel;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace Cardinal.Compiler
{
    public struct DecodedOpCode
    {
        public int InstrOffset;
        public OpCode Op;
        public MethodBase Method;
        public FieldInfo Field;
        public MemberInfo Member;
        public Type Type;
        public byte I8;
        public short I16;
        public int I32;
        public long I64;
        public float R32;
        public double R64;
        public string Str;
        public int[] BrTargets;
        public int[] ResolvedBrTargets;
    }

    public class Decompiler<T> where T : IAsmEntry, new()
    {
        private readonly TypeDatabase database;
        private readonly ICodeGenerator<T> codeGenerator;

        public AsmCollection<T> AsmCollection { get; }

        public Decompiler(TypeDatabase db, IAsmPlatform platform, ICodeGenerator<T> generator)
        {
            database = db;
            AsmCollection = new AsmCollection<T>(platform);
            codeGenerator = generator;
        }

        public void Decompile(Assembly asm)
        {
            foreach (Type t in asm.GetTypes())
            {
                Decompile(asm, t);
            }
        }

        public void Decompile(Assembly asm, Type t)
        {
            foreach (var m in t.GetAllConstructors())
            {
                Decompile(asm, m);
            }

            foreach (var m in t.GetAllMethods())
            {
                Decompile(asm, m);
            }
        }

        public void DecompileDB()
        {
            var types = database.GetTypes();
            foreach (var t in types)
                Decompile(t.Assembly, t);
        }

        private void Decompile(Assembly asm, MethodBase og, MethodBase mthd, MethodBody mBody)
        {
            var il_b = mBody.GetILAsByteArray();

            var opcodes_fields = typeof(System.Reflection.Emit.OpCodes).GetFields();
            var opcodes = new OpCode[opcodes_fields.Length];
            for (int i = 0; i < opcodes.Length; i++)
                opcodes[i] = (OpCode)opcodes_fields[i].GetValue(null);

            var dec_ops = new List<DecodedOpCode>();

            var idx = 0;
            while (idx < il_b.Length)
            {
                //Find current opcode
                bool opcode_found = false;
                bool in_prefix = false;
                if (il_b[idx] == OpCodes.Prefix1.Value)
                {
                    in_prefix = true;
                }

                for (int i = 0; i < opcodes.Length; i++)
                {
                    if ( (!in_prefix && opcodes[i].Value == il_b[idx]) | (in_prefix && opcodes[i].Value == (short)(il_b[idx + 1] | 0xfe00)) )
                    {
                        var opc = opcodes[i];
                        opcode_found = true;
                        var dec_opc = new DecodedOpCode()
                        {
                            Op = opc,
                            InstrOffset = idx,
                        };

                        switch (opc.OperandType)
                        {
                            case OperandType.InlineField:
                                {
                                    var tkn = BitConverter.ToInt32(il_b, idx + opc.Size);
                                    dec_opc.Field = mthd.Module.ResolveField(tkn);

                                    idx += sizeof(int) + opc.Size;
                                }
                                break;
                            case OperandType.InlineMethod:
                                {
                                    var tkn = BitConverter.ToInt32(il_b, idx + opc.Size);
                                    dec_opc.Method = mthd.Module.ResolveMethod(tkn);

                                    idx += sizeof(int) + opc.Size;
                                }
                                break;
                            case OperandType.InlineSig:
                                {
                                    var tkn = BitConverter.ToInt32(il_b, idx + opc.Size);
                                    var sig = mthd.Module.ResolveSignature(tkn);

                                    idx += sizeof(int) + opc.Size;
                                    throw new NotImplementedException("InlineSig support not implemented.");
                                }
                                break;
                            case OperandType.InlineString:
                                {
                                    var tkn = BitConverter.ToInt32(il_b, idx + opc.Size);
                                    dec_opc.Str = mthd.Module.ResolveString(tkn);

                                    idx += sizeof(int) + opc.Size;
                                }
                                break;
                            case OperandType.InlineTok:
                                {
                                    var tkn = BitConverter.ToInt32(il_b, idx + opc.Size);
                                    dec_opc.Member = mthd.Module.ResolveMember(tkn);

                                    idx += sizeof(int) + opc.Size;
                                }
                                break;
                            case OperandType.InlineType:
                                {
                                    var tkn = BitConverter.ToInt32(il_b, idx + opc.Size);
                                    dec_opc.Type = mthd.Module.ResolveType(tkn);

                                    idx += sizeof(int) + opc.Size;
                                }
                                break;
                            case OperandType.InlineBrTarget:
                                {
                                    var off = BitConverter.ToInt32(il_b, idx + opc.Size) + idx + opc.Size + sizeof(int);
                                    dec_opc.BrTargets = new int[] { off };

                                    idx += sizeof(int) + opc.Size;
                                }
                                break;
                            case OperandType.ShortInlineBrTarget:
                                {
                                    var off = unchecked((sbyte)il_b[idx + 1]) + idx + opc.Size + sizeof(byte);
                                    dec_opc.BrTargets = new int[] { off };

                                    idx += sizeof(byte) + opc.Size;
                                }
                                break;
                            case OperandType.InlineVar:
                                dec_opc.I16 = BitConverter.ToInt16(il_b, idx + 1);

                                idx += sizeof(short) + opc.Size;
                                break;
                            case OperandType.ShortInlineVar:
                                dec_opc.I8 = il_b[idx + 1];

                                idx += sizeof(byte) + opc.Size;
                                break;
                            case OperandType.InlineI:
                                dec_opc.I32 = BitConverter.ToInt32(il_b, idx + 1);

                                idx += sizeof(int) + opc.Size;
                                break;
                            case OperandType.InlineI8:
                                dec_opc.I64 = BitConverter.ToInt64(il_b, idx + 1);

                                idx += sizeof(long) + opc.Size;
                                break;
                            case OperandType.InlineR:
                                dec_opc.R64 = BitConverter.ToDouble(il_b, idx + 1);

                                idx += sizeof(double) + opc.Size;
                                break;
                            case OperandType.ShortInlineI:
                                dec_opc.I8 = il_b[idx + 1];

                                idx += sizeof(byte) + opc.Size;
                                break;
                            case OperandType.ShortInlineR:
                                dec_opc.R32 = BitConverter.ToSingle(il_b, idx + 1);

                                idx += sizeof(float) + opc.Size;
                                break;
                            case OperandType.InlineSwitch:
                                {
                                    var sw_cnt = BitConverter.ToInt32(il_b, idx + 1);
                                    dec_opc.BrTargets = new int[sw_cnt];
                                    for (int j = 0; j < sw_cnt; j++)
                                    {
                                        dec_opc.BrTargets[j] = BitConverter.ToInt32(il_b, idx + 1 + sizeof(int) * (j + 1)) + idx + /*opc.Size*/ (1 + (sw_cnt + 1) * sizeof(int));
                                    }
                                    Console.WriteLine($"[SWITCH] {opc.Size} {(sw_cnt + 1) * sizeof(int)}");

                                    idx += sizeof(int) * (sw_cnt + 1) + opc.Size;
                                }
                                break;
                            case OperandType.InlineNone:
                                idx += opc.Size;
                                break;
                            default:
                                throw new NotImplementedException("Unknown OperandType.");
                        }

                        Console.WriteLine($"\t\t{opc.Name} {opc.OperandType}");
                        dec_ops.Add(dec_opc);
                        break;
                    }
                }
                if (!opcode_found)
                    throw new Exception("Unrecognized Opcode.");
            }

            //lower the opcodes to native IL
            //TODO: Translate token references and generate output assembly, implementing generators per opcode
            for (int i = 0; i < dec_ops.Count; i++)
            {
                var d = dec_ops[i];
                if (d.BrTargets != null)
                {
                    d.ResolvedBrTargets = new int[d.BrTargets.Length];

                    for (int k = 0; k < d.BrTargets.Length; k++)
                        for (int j = 0; j < dec_ops.Count; j++)
                            if (dec_ops[j].InstrOffset == d.BrTargets[k])
                            {
                                d.ResolvedBrTargets[k] = j;
                                break;
                            }
                }
                dec_ops[i] = d;
            }

            if (!AsmCollection.FunctionExists(og))
            {
                AsmCollection.CreateFunction(og, mthd);
                var ent = AsmCollection.GetFunction(og);
                codeGenerator.Generate(dec_ops.ToArray(), ent);
            }
        }

        public void Decompile(Assembly asm, ConstructorInfo cInfo)
        {
            Console.WriteLine($"{cInfo.ReflectedType.FullName}.ctor");
            Console.WriteLine($"\t{database.ResolveConstructor(cInfo).ReflectedType.FullName}");

            var resInfo = database.ResolveConstructor(cInfo);
            var mBody = resInfo.GetMethodBody();
            if (mBody != null)
            {
                Decompile(asm, cInfo, resInfo, mBody);
            }
        }

        public void Decompile(Assembly asm, MethodInfo mInfo)
        {
            Console.WriteLine($"{mInfo.ReflectedType.FullName}.{mInfo.Name} base:{mInfo.DeclaringType.FullName}");
            Console.WriteLine($"\t{database.ResolveMethod(mInfo).ReflectedType.FullName}");

            var resInfo = database.ResolveMethod(mInfo);
            var mBody = resInfo.GetMethodBody();
            if (mBody != null)
            {
                Decompile(asm, mInfo, resInfo, mBody);
            }
        }
    }
}
