﻿using System.Reflection;

namespace Cardinal.Compiler
{
    public interface IAsmPlatform
    {
        IStackAllocator CreateStackAlloc(MethodBase og, MethodBase mthd);
        int AddString(string s);
        byte[] CompileStringTable();
    }
}
