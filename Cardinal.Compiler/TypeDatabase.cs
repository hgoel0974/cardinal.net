using System.Collections.Generic;
using System.Reflection;
using System;
using Cardinal.Corlib;
using System.Linq;

namespace Cardinal.Compiler
{
    public class TypeDatabase
    {
        private readonly Dictionary<Type, Type> TypeMappings;
        private readonly Dictionary<int, Type> MetadataTypes;
        private readonly Dictionary<int, MethodBase> MetadataMethods;

        public TypeDatabase()
        {
            TypeMappings = new Dictionary<Type, Type>();
            MetadataTypes = new Dictionary<int, Type>();
            MetadataMethods = new Dictionary<int, MethodBase>();
        }

        //TODO: Read and build type mappings from the corlib
        public void ParseCorlib()
        {
            ParseAssembly(typeof(TypeMapAttribute).Assembly);
        }

        public void ParseAssembly(Assembly asm)
        {
            var types = asm.GetTypes();
            foreach (Type t in types)
            {
                foreach (object customAttrObj in t.GetCustomAttributes(typeof(TypeMapAttribute), false))
                {
                    //Register all metadata tokens
                    MetadataTypes[t.MetadataToken] = t;
                    foreach (var m in t.GetAllMethods())
                    {
                        MetadataMethods[m.MetadataToken] = m;
                    }

                    foreach (var m in t.GetAllConstructors())
                    {
                        MetadataMethods[m.MetadataToken] = m;
                    }

                    if (customAttrObj is TypeMapAttribute typeMap)
                    {
                        TypeMappings[typeMap.TargetType] = t;
                    }
                }
            }
        }

        public Type[] GetTypes()
        {
            return TypeMappings.Keys.ToArray();
        }

        public Type ResolveType(Type t)
        {
            if (TypeMappings.ContainsKey(t))
                return TypeMappings[t];
            return t;
        }

        public Type LookupTypeByMetadataToken(int token)
        {
            return MetadataTypes[token];
        }

        public MethodBase LookupMethodByMetadataToken(int token)
        {
            return MetadataMethods[token];
        }

        private bool MatchParameters(ParameterInfo[] a, ParameterInfo[] b)
        {
            if (a.Length != b.Length)
                return false;

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i].IsIn != b[i].IsIn)
                    return false;
                if (a[i].IsOut != b[i].IsOut)
                    return false;
                if (a[i].IsOptional != b[i].IsOptional)
                    return false;
                if (a[i].IsRetval != b[i].IsRetval)
                    return false;
                if (ResolveType(a[i].ParameterType) != ResolveType(b[i].ParameterType))
                    return false;
            }
            return true;
        }

        private bool MatchMethod(MethodInfo a, MethodInfo b)
        {
            if (a.IsStatic != b.IsStatic)
                return false;
            if (a.IsVirtual != b.IsVirtual)
                return false;
            if (ResolveType(a.ReturnType) != ResolveType(b.ReturnType))
                return false;
            if (!MatchParameters(a.GetParameters(), b.GetParameters()))
                return false;
            return true;
        }

        private bool MatchConstructor(ConstructorInfo a, ConstructorInfo b)
        {
            if (a.IsStatic != b.IsStatic)
                return false;
            if (a.IsVirtual != b.IsVirtual)
                return false;
            if (!MatchParameters(a.GetParameters(), b.GetParameters()))
                return false;
            return true;
        }

        public MethodInfo ResolveMethod(MethodInfo src)
        {
            //Match method name, static etc and parameters
            var t = ResolveType(src.DeclaringType);
            if (t == src.DeclaringType)
                return src;
            foreach (var m in t.GetAllMethods())
                if (MatchMethod(m, src))
                    return m;
            return src;
        }

        public ConstructorInfo ResolveConstructor(ConstructorInfo src)
        {
            //Match static etc and parameters
            var t = ResolveType(src.DeclaringType);
            if (t == src.DeclaringType)
                return src;
            foreach (var m in t.GetAllConstructors())
                if (MatchConstructor(m, src))
                    return m;
            return src;
        }
    }
}
